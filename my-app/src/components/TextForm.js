import React, { useState } from "react";

export default function TextForm(props) {

    const handleUpClick=()=>{
        let newtext=text.toUpperCase();
        setText(newtext);
    }

    const handleDownClick=()=>{
        let newtext=text.toLowerCase();
        setText(newtext);
    }

    const handleOnChange=(event)=>{
        setText(event.target.value);
    }

    const[text,setText]=useState('');
    return (
        <>
        <div className="container">
            <h1>{props.heading}</h1>
            <div className="mb-3">
                <textarea className="form-control" id="myBox" rows="8" value={text} onChange={handleOnChange}></textarea>
            </div>
            <button className="btn btn-primary mx-1" onClick={handleUpClick}>Convert to Uppercase</button>
            <button className="btn btn-primary mx-1" onClick={handleDownClick}>Convert to Lowercase</button>
        </div>
        <div className="container my-3">
            <h2>Your text summary</h2>
            <p><b>{text.split(" ").length} words and {text.length} characters</b></p>
            <p>{0.08 * text.split(" ").length} minute to read</p>
            <h2>Preview</h2>
            <p>{text}</p>
        </div>
        </>
    );
}
